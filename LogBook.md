Journal de Bord
==
**02/02/2018** :<br>
*Alidor* :Aujoud'hui j'ai commencé le projet en faisant la lecture de tout ce qui à déja était fait par l'ancien groupe puis j'ai effectuer quelques commentaire pour une meilleure compréhension. Avec Aviran on a commencer par faire la répartition des taches et j'ai défini des issues. <br>
*Tetia* : 
        - Relecture intégrale du projet précédent
        - Ajout de commentaires pour endre le code final plus compréhensible
        - Découverte de l'application android précédemment développée sur app inventor 
*Sery* : 

**08/02/2018** : <br>
*Alidor* : Recherche sur le module gps + comprehension du code.<br>
*Tetia* : 
		- Définition du projet dans le readme
		- Test du projet initial/ debug<br>
*Sery* : 

**13/02/2018** : <br>
*Alidor* : Compréhension du module gps, création d'un programme gps ainsi que création d'issues. <br>
*Tetia* : 
		- Changement de méthode d'identification --> bouton + potentiomettre
		- Développement d'un code permettant la saisi d'un identifiant avec un bouton et un potentiomettre (en cours)
*Sery* : 

**20/02/2018** : <br>
*Alidor* : Aujourd'hui nous avons décider d'utiliser le buzzer pour au cas ou la clé ne serait pas localisé. j'ai pu tester le code du buzzer ainsi que réorganiser l'architecture du readme.md <br>
*Tetia* : 
*Sery* : 

**23/02/2018** : <br>
*Alidor* : 
*Tetia* : ajustement code indentification<br>
*Sery* : 

**28/02/2018** : <br>
*Alidor* : Création du readme + commentaires du code..
*Tetia* : 
*Sery* : 