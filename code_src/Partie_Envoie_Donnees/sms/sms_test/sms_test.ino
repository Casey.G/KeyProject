#include <LGSM.h>                                                 // Include Linkit ONE GSM library
String message;                                                   // variable qui va contenir notre message

void setup() {
  
  // put your setup code here, to run once:

  Serial.begin(115200);                 // On intialise la vitesse de communication à 115200 bauds
  
  while (!LSMS.ready())                 // On attend que le module sms soit prèt
  {
    delay(1000);                        // Si le moduule sms n'est pas prèt on attend 1 seconde puis on réessaie
  }
  
  Serial.println("Sim initialized");    // Nous avertis quand la sim est initialisée

  LSMS.beginSMS("0692094624");          // Ce numéro sera celui du secrétariat

}

void loop() {
  
  // Put your main code here, to run repeatedly:

  message = "je ne sais pas où je me trouve pour l'instant, je suis un teste" ;        // on défini le contenu de notre message

  LSMS.print(message);                  // Péparation du sms à contenir notre variable message

  for(int i=0 ; i<4 ; i++){          // On envoie 4 messages de test
    if (LSMS.endSMS())                  // On envoie le message et on vérifie si l'envoie est bien effectué 
    {
      Serial.println("SMS sent");    // Si l'envoie aboutie, on affiche "sms sent"
    }
    else
    {
      Serial.println("SMS is not sent");// Sinon "SMS is not sent"
    }
  }
 
}
