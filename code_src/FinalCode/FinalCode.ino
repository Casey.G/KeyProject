/*===================================================================================================
Decription du projet : 
composants: Un LCD, Une LED d'alerte, Une batterie, Une linkit One
Fonctionnalités utilisées: WiFi, Bluetooth 
=====================================================================================================*/

//#include <LBT.h>          //librairie dédiée au bluetooth
//#include <LBTServer.h>    //librairie dédiée au serveur bluetooth  de la carte linkit one


#include <LWiFi.h>          //on inclut toutes les bibliotèques nécessaires
#include <rgb_lcd.h>        //on inclut la libraire pour la gestion de l'écran LCD
#include <LBattery.h>       //on inclut la librairie pour la gestion de la batterie
#include <LGSM.h>           // Inclut la librairie pour l'utilisation des fonctions GSM
#include <Wire.h>           
//include de la partie Bluetooth

#include <LGPS.h> // librairiz dédiée au gps
#define SPP_SVR "keyProject" //On défini le nom du serveur Bluetooth

#include <LWiFiClient.h>

LWiFiClient c;      //objet qui va nous permettre de nous connecter (objet client)

rgb_lcd lcd;                //on crée un objet rgb_lcd pour pouvoir contrôler le LCD
char buff[256];             //on déclare un buffer (ici sous la forme d'un tableau) ayant 256 espaces mémoire
int nivBat;                 //entier qui va contenir le niveau de la batterie
bool charge;                 //pour savoir si on est en train de charger la batterie ou non (non utilisé pour l'instant) 

const int colorR = 235;     //pour définir le code couleur
const int colorG = 245;
const int colorB = 255;

//variable pour contenir les entiers qui composent le numéro étudiant
int chiffre=0;
const int potPin = A0;
const int btnPin = A1;

int rangNumEtudiant = 0; //pour indéxer les chiffres du numéro étudiant
String numEtudiant = ""; //va contenir la totalité du numéro étudiant

int buttonPushCounter = 0;   // counter for the number of button presses
int buttonState = 0;         // current state of the button
int lastButtonState = 0;     // previous state of the button
boolean empruntee = false;

//int read_size = 0;(now unused)        //variable qui va contenir la donnée envoyer par l'appareil connecté à la carte linkit (sur son serveur bluetooth)
const int LEDPIN = 2;    //pin sur laquelle est reliée la led
IPAddress ipServ;

//flag pour executer une fonction une seule fois
int runOnce;

void setup() {
  runOnce = 0; //on initialise notre flag à 0
  Serial.begin(9600);           //on démarre la communication série
  Serial.print("Setup en cours...");
  //Partie Batterie
  pinMode(LEDPIN, OUTPUT);          //définie le pin 13 comme une sortie 
   //on définit les pins sur lesquels sont reliés le potentiomètre et le bouton
  pinMode(potPin,INPUT);
  pinMode(btnPin,INPUT_PULLUP);
  
  //On définit le nombre de colonnes et de lignes que l'on aura pour le LCD
  
  lcd.begin(16, 2);
    
  lcd.setRGB(colorR, colorG, colorB); //définition des couleurs
  
  Serial.println("Ecran prêt");
    
  // Affichage du niveau de batterie sur l'écran LCD
  lcd.print ("Battery: ");
  lcd.print(LBattery.level());  //on affiche le niveau de la batterie
  lcd.print ("%");
  
  //Initialisation du module sms
  while (!LSMS.ready())                 // On attend que le module sms soit prèt
  {
    delay(1000);                        // Si le moduule sms n'est pas prèt on attend 1 seconde puis on réessaie
  }
  Serial.println("Sim initialized");    // Nous avertis quand la sim est initialisée
  LSMS.beginSMS("0692094624");          // Ce numéro sera celui du secrétariat
  
  //activation du wifi
  LWiFi.begin();
  Serial.println("démarage serveur wifi");                  
  
}
void loop() {  
  //partie empreint//
  //on lit la valeur du bouton
  lireBouton();
  //on lit la valeur du potentiomètre
  chiffre = map(analogRead(potPin), 0,1023, 0,9);

    //=======saisi du numéro étudiant==============
        if(rangNumEtudiant > 7){ //si le numéro est complet, on valide l'empreint 
          emprunt();
         
        }else{
          buttonState = digitalRead(btnPin); //on lit la valeur du bouton
          lcd.setCursor(0, 0);
          lcd.setRGB(colorR, colorG, colorB); //définition des couleurs
          lcd.print("Num etudiant: ");
          
          //on se place sur la deuxième ligne de l'écran
          lcd.setCursor(rangNumEtudiant, 1);
          lcd.print(chiffre);//on affiche le chiffre que pointe actuellement le potentiomètre
          if(buttonState != lastButtonState){ //si il y a un changement d'état du bouton
            if(buttonState == LOW){
               rangNumEtudiant++;
               numEtudiant += chiffre;
               //Serial.println(numEtudiant);//pour afficher le numéro étudiant qui se construit
            }
            lastButtonState = buttonState;
          }
        }
 
  
 //Partie Batterie//
  //sprintf(buff,"battery level = %d", LBattery.level() ); //affichage du niveau de batterie sur le moniteur série
  //Serial.println(buff);
  nivBat=LBattery.level();
  //sprintf(buff,"is charging = %d",LBattery.isCharging() ); //affichage de l'état de chagement de la batterie (oui ou non?)
  //Serial.println(buff);
  
  if (nivBat<50) {                     //si le niveau de la batterie est inférieur à 100% on fait clignoter la LED et on fait buzzer le buzzeur
   lcd.setRGB(255, 0, 0);               //on change la couleur de l'écran
   signaler();
  } 
 //delay(1000); 
 
}
 
void printCurrentNet() {                  //fonction qui va nous permettre d'afiicher les détails de l'AP sur lequel on est connecté
  
  // on afficher le SSID de l'AP sur lequel on est connecté
  Serial.print("SSID: ");
  Serial.println(LWiFi.SSID());
  // On affiche l'adresse MAC de l'AP sur lequel on est connecté
  byte bssid[6];
  LWiFi.BSSID(bssid);    
  Serial.print("BSSID: ");

  // print your WiFi shield's IP address:
  IPAddress ip = LWiFi.localIP();
  setIP(ip);
  Serial.print("IP Address: ");
  Serial.println(ip);

  
  for (int compteur =0; compteur<5; compteur++){        //on affiche "un a un" chaque partie de l'adresse MAC de l'AP 
    Serial.print(bssid[compteur],HEX);
    Serial.print(":");                    //on affiche sous la même forme que si on était sur un pc AA:AA:AA:AA:AA:AA
  }
  Serial.println(bssid[5],HEX);               //on affiche la dernière partie de l'adresse MAC avec un retour à la ligne
  long rssi = LWiFi.RSSI();                 //on stocke la valeur de la puissance du signal reçue de l'AP
  Serial.print("signal strength (RSSI):");          //on affiche la puissance du signal de l'AP
  Serial.println(rssi);
}
void signaler() {     //fonction pour faire clignoter la LED et activer le buzzer
  digitalWrite(LEDPIN, HIGH);   //on allume la LED et le buzzer
  delay(1000);               //on attend 1s
  digitalWrite(LEDPIN, LOW);    //on éteint la LED et le buzzer
  delay(1000);               //on attend une nouvelle fois 1s
  
}
//afin d'obtenir l'adresse ip de notre serveur en dynamique nous avons ces deux fonctions:
void setIP(IPAddress ip){
  ipServ = ip;
}
IPAddress getIP(){
  return ipServ;
}

boolean emprunt(){ // méthode pour l'empreint de la clé 
  //on lit la valeur du bouton
  lireBouton(); 
  lcd.clear(); // méthode qui permet de 
  lcd.print("empreint valide"); // on affiche que c'est la bonne empreinte 
  lcd.setRGB(0, colorG, 155);
  
  if(runOnce == 0){
     String message = "clef en projDoc empruntee par " + numEtudiant;
     alertSms(message); //ce message sera par la suite modifié selon la position de la clé, cette modification se fera lors de l'implémentation de la partie localisation wifi . 
  }
  return true;  
}

void lireBouton(){
  buttonState = digitalRead(btnPin);
}

void alertSms(String msg){
  LSMS.print(msg);                  // Péparation du sms à contenir notre variable message
    if (LSMS.endSMS())                  // On envoie le message et on vérifie si l'envoie est bien effectué 
    {
        Serial.println("SMS sent");    // Si l'envoie aboutie, on affiche "sms sent"
        runOnce = 1;
    }
    else
    {
      Serial.println("SMS is not sent");// Sinon "SMS is not sent"
    } 
}
//Partie Bluetooth (unused now)
 /*
 bool success = LBTServer.begin((uint8_t*)SPP_SVR); //pour vérifier si le Bluetooth a bien été lancé
  if( !success )
  {
      Serial.printf("Cannot begin Bluetooth Server successfully\n");
  }
  else
  {
      Serial.printf("Bluetooth Server begin successfully\n"); //on annonce que le serveur a bien été lancé
  }
 
  // On attend pour voir si on a un client qui se connecte
  bool connected = LBTServer.accept(20); //on met un timeout de 20secondes, le temps qu'a l'utilisateur pour se connecter avant le prochain cycle
 int sent =0;
  if( !connected )
  {
      Serial.printf("No connection request yet\n");
      // si aucun périphérique n'est connecté
      sent=1; //on met sent à 1 pour dire que l'on ne va pas à recevoir de données
  }
  else
  {
      Serial.printf("Connected\n"); //si un périphérique est connecté on le notifie sur le moniteur série
  }
  
  if (!sent)
  {
      char buffer[32] = {0};
      
      memset(buffer, 0, sizeof(buffer));
        delay (10000); //on attends 10 secondes et si l'utilisateur a rien tapé, on arrête la communication
        if(LBTServer.available())
        {
          read_size = LBTServer.readBytes((uint8_t*)buffer, 32); //on va lire ce qu'envoie le téléphone
        }
      
      Serial.printf("%s[%d]\n",  buffer, read_size);//on affiche ce qui est envoyé par le téléphone sur le moniteur série
     
      if(c.connect(getIP(), 8888)){  //si un client se connecte au serveur wifi, on envoie les données précédement saisies par l'uttilisateur
       c.println(buffer);

     }
  }
  LBTServer.end(); //on arrête le serveur Bluetooth
  delay(10000);
  LWiFi.end();                           // on éteins le wifis

  
}
  */

  

