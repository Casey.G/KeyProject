The KeyProject
==

Objectif 
-----------
The KeyProject is aimed to keep track of the keys at the
Networks and Telecommnication departement.

(EN)
- A linkit one is stuck to the key that we take
- When the key is taken, the student enter his student number on it by turning a potentiometer and pushing on a button for each number
- The key tries to estimate his location according to the MAC adress of the two closest access point around him every 30 seconds (based on the machine time) and once its done, it keeps in memorie the location
- If the key recieve a text message from the secretariat, it sends back his location 
- If the key can't detect any wifi access point around him for a certain amount of time, an alert is sent by sms
- If the battery goes down, an alarm rings too
- The student pushes the reset button to land the keys or re-enter his student number

(FR)
- La carte linkit one est reliée à la clé que nous prenons
- Lorsque la clé est prise, l'étudiant y inscrit son numéro d'étudiant via un bouton et un potentiomètre
- La clé estime sa position grâce aux adresses MAC des deux points d'accès wifi les plus proches d'elle toutes les 30 secondes (en se basant sur le temps machine, pas de "delay"), ceci fait, elle garde sa localisation en mémoire
- Si la clé reçoit un sms provenant du secretariat, elle répond en envoyant sa position précédemment calculée
- Si la clé ne peut pas détecter de point d'accès wifi pendant un certain temps, un alerte est envoyée par sms
- Si le niveau de la batterie devient trop faible (<30%), une alarme sonne
- L'étudiant apuie sur le bouton reset pour rendre la clé

Installation
-----------
Pour installer ce logiciel vous devez cloner ce repository : <br> 
<table border=1>  
    <td> git clone https://gitlab.com/430x414squad/KeyProject.git </td></table><br>
**Matériel** : 
-*Carte Linkit One*
-*Module Linkit wifi *
-*Groove Shield for Linkit One*
-*Module Linkit GPS*
-*Ecran lcd Groove*
-*Batterie Linkit Li-ion 3.7v,9wh,1050mAh*
-*Module Groove Button*

**ETAPE 1** :
![](https://i.imgur.com/jVbOTeg.jpg)
*Nous allons connecter le Grove Shield avec la carte Linkit One.*

**ETAPE 2** : <br>
![](https://i.imgur.com/odIwf3y.jpg)<br>
*Maintenant nous allons connecter l'antenne Wifi/Gsm.*

**ETAPE 3** : <br>
![](https://i.imgur.com/X5cHG9Q.jpg)<br>
*Nous allons connecter l'écran ansi qur le button et le potentiomètre.*


**ETAPE 4** : <br>
![](https://i.imgur.com/fQApK9r.jpg)<br>
*Enfin nous transferrons le code final dans le boitier et nous alimentons la carte.* <br>


Auteurs
-----------
- Casey G (c.grondin@rt-iut.re)
- Ludovic A (l.alidor@rt-iut.re)
- Aviran T (a.tetia@rt-iut.re)
- Victor S (v.sery@rt-iut.re)
- Dylan F (d.fontaine@rt-iut.re)
- Eitcher H (e.hoareau@rt-iut.re)
- Alban N (a.nimba@rt-iut.re)
<br>

Lisence
-----------
 - **IUT DE LA REUNION**